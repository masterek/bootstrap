#!/bin/bash -e
# Redirect stdout ( > ) into a named pipe ( >() ) running "tee"
exec > >(tee -i logfile.txt)
# Without this, only stdout would be captured - i.e. your
# log file would not contain any error messages.
# SEE (and upvote) the answer by Adam Spiers, which keeps STDERR
# as a separate stream - I did not want to steal from him by simply
# adding his answer to mine.
exec 2>&1


## Perform OS updates
#sudo apt-get update
#sudo apt-get -y upgrade

# Install system components
sudo apt-get update
sudo apt-get install curl gnupg apt-transport-https

# Import the public repository GPG keys
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
# Register the Microsoft Product feed
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-stretch-prod stretch main" > /etc/apt/sources.list.d/microsoft.list'
# Update the list of products
sudo apt-get update
# Install PowerShell
sudo apt-get install -y powershell

## Install git
sudo apt-get -y install git

## Install puppet
#wget https://apt.puppetlabs.com/puppetlabs-release-pc1-VERSION.deb
#wget -O /tmp/puppetlabs.deb http://apt.puppetlabs.com/puppetlabs-release-`lsb_release -cs`.deb
#dpkg -i /tmp/puppetlabs.deb
#apt-get update
sudo apt-get -y install puppet

# Clone the 'puppet' repo
#cd /etc
#mv puppet/ puppet-bak
#git clone http://your_git_server_ip/username/puppet.git /etc/puppet

# Run Puppet initially to set up the auto-deploy mechanism
#puppet apply /etc/puppet/manifests/site.pp